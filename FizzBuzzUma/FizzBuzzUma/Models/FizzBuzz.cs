﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.Text;
namespace FizzBuzzUma.Models
{
    public class FizzBuzz
    {
       [Required][Range(1,1000)]public int Value { get;  set; }
      public bool IsDivisableByThree(int input)
       { return (input % 3) == 0; }
      public bool IsDivisableByFive(int input)
      { return (input % 5) == 0; }
      public string GetTextForNubmerWithColor(int input)
      {
          if(IsDivisableByFive(input) && IsDivisableByThree(input) )
          {
              return "<font color=blue>fizz</font> <font color=green>buzz</font>";
          }
          if (IsDivisableByFive(input))
          { return "<font color=green>buzz</font>"; }
          if (IsDivisableByThree(input))
          { return "<font color=blue>fizz</font>"; }
          else
          { return input.ToString() ; }
      }
        public string PrintAll(int inputValue)
      {
          StringBuilder sb = new StringBuilder();
            for(int i=1;i<=inputValue ;++i )
            {
                sb.AppendFormat("{0}", GetTextForNubmerWithColor(i));
            //sb.AppendFormat(Environment.NewLine);
                sb.Append("<BR>" );
            }
            //if (DateTime.Now.DayOfWeek == DayOfWeek.Wednesday)
            //{
            // sb.Replace("fizz", "wizz"); 
            // sb.Replace("buzz", "wuzz"); 
            //}
            
            return sb.ToString();
      }
        public string PrintAllWednesday(int inputValue,DayOfWeek day)
        {
            StringBuilder sb = new StringBuilder();
            for (int i = 1; i <= inputValue; ++i)
            {
                sb.AppendFormat("{0}", GetTextForNubmerWithColor(i));
            }
            if (day == DayOfWeek.Wednesday)
            {
                sb.Replace("fizz", "wizz");
                sb.Replace("buzz", "wuzz");
            }

            return sb.ToString();
        }
    }
}