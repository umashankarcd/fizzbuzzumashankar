﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using FizzBuzzUma.Models;
namespace FizzBuzzTest
{
    [TestClass]
    public class FizzBuzzUnitTest
    {
        [TestMethod]
        public void ThreeIsDivisableByThreeTest()
        {
            FizzBuzz fb = new FizzBuzz();
            bool divisible = fb.IsDivisableByThree(3);
            Assert.AreEqual(divisible,true);
        }
        [TestMethod]
        public void FiveIsDivisableByFiveTest()
        {
             FizzBuzz fb = new FizzBuzz();
            bool divisible = fb.IsDivisableByFive(5);
            Assert.AreEqual(divisible,true);
        }

        [TestMethod]
        public void TextForDivisableByThreeIsFizzTest()
        {
            FizzBuzz fb = new FizzBuzz();
            string strText = fb.GetTextForNubmerWithColor(3);
            Assert.AreEqual(strText, "<font color=blue>fizz</font>");
        }

        [TestMethod]
        public void TextForDivisableByFiveIsFizzTest()
        {
            FizzBuzz fb = new FizzBuzz();
            string strText = fb.GetTextForNubmerWithColor(5);
            Assert.AreEqual(strText, "<font color=green>buzz</font>");
        }
        [TestMethod]
        public void TextForDivisableByThreeAndFiveIsFizzBuzzTest()
        {
            FizzBuzz fb = new FizzBuzz();
            string strText = fb.GetTextForNubmerWithColor(15);
            Assert.AreEqual(strText, "<font color=blue>fizz</font> <font color=green>buzz</font>");
        }

        [TestMethod]
        public void TextForNotDivisableByThreeNorFiveIsItselfText()
        {
            FizzBuzz fb = new FizzBuzz();
            string strText = fb.GetTextForNubmerWithColor(4);
            Assert.AreEqual(strText, "4");
        }
        [TestMethod]
        public void PrintAllTest()
        {
            FizzBuzz fb = new FizzBuzz();
            string strAllText = fb.PrintAll(15);
            Assert.AreEqual(strAllText, "1<BR>2<BR><font color=blue>fizz</font><BR>4<BR><font color=green>buzz</font><BR><font color=blue>fizz</font><BR>7<BR>8<BR><font color=blue>fizz</font><BR><font color=green>buzz</font><BR>11<BR><font color=blue>fizz</font><BR>13<BR>14<BR><font color=blue>fizz</font> <font color=green>buzz</font><BR>");
        }
        [TestMethod]
        public void PrintAllWednesdayTest()
        {
            FizzBuzz fb = new FizzBuzz();
            string strAllText = fb.PrintAllWednesday(15,DayOfWeek.Wednesday);
            Assert.AreEqual(strAllText, "12<font color=blue>wizz</font>4<font color=green>wuzz</font><font color=blue>wizz</font>78<font color=blue>wizz</font><font color=green>wuzz</font>11<font color=blue>wizz</font>1314<font color=blue>wizz</font> <font color=green>wuzz</font>");
        }
    }
}
